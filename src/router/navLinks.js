import router from './../router/index'

import { 
    PRODUCT,
    BRAND,
    BRANCH,
    CATEGORY,
    SUPPLIER,

    PURCHASE_ORDER,
    PURCHASE_INBOUND,
    PURCHASE_RETURN,
    PURCHASE_RETURN_OUTBOUND,
    PAYMENT,

    SALES,
    SALES_OUTBOUND,
    SALES_INBOUND,
    SALES_RETURN,
    SALES_RETURN_INBOUND,
    COUNTER
} from './routes.type'

const _navLinks = [
    {
        label:'label.data',
        icon:'',
        content:[
            {
                path:PRODUCT,
                label:"label.product",
                icon:'',
                content:[]
            },
            {
                path:BRAND,
                label:'label.brand',
                icon:'',
                content:[],
            },
            {
                path:BRANCH,
                label:'label.branch',
                icon:'',
                content:[],
            },
            {
                path:CATEGORY,
                label:'label.category',
                icon:'',
                content:[],
            },
            {
                path:SUPPLIER,
                label:'label.supplier',
                icon:'',
                content:[],
            }
        ],
    },
    {
        label:'label.purchase_and_other_payment',
        icon:'',
        content:[
            {
                path:PURCHASE_ORDER,
                label:'label.purchase_order',
                icon:'',
                content:[],
            },
            {
                path:PURCHASE_INBOUND,
                label:'label.purchase_inbound',
                icon:'',
                content:[],
            },
            {
                path:PURCHASE_RETURN,
                label:'label.purchase_return',
                icon:'',
                content:[],
            },
            {
                path:PURCHASE_RETURN_OUTBOUND,
                label:'label.purchase_return_outbound',
                icon:'',
                content:[],
            },
            {
                path:PAYMENT,
                label:'label.payment',
                icon:'',
                content:[],
            }
        ],
    },
    {
        label:'label.sales_and_other_income ',
        icon:'',
        content:[
            {
                path:SALES,
                label:'label.sales',
                icon:'',
                content:[],
            },
            {
                path:SALES_OUTBOUND,
                label:'label.sales_outbound',
                icon:'',
                content:[],
            },
            {
                path:SALES_INBOUND,
                label:'label.purchase_inbound',
                icon:'',
                content:[],
            },
            {
                path:SALES_RETURN_INBOUND,
                label:'label.sales_return_inbound',
                icon:'',
                content:[],
            },
            {
                path:COUNTER,
                label:'label.counter',
                icon:'',
                content:[],
            }
        ],
    },

]


export default {
    links (cb) {
        setTimeout(() => cb(_navLinks), 100)
    },
}