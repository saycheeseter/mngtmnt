export const PRODUCT = '/product'
export const BRAND = '/brand'
export const BRANCH = '/branch'
export const CATEGORY = '/category'
export const SUPPLIER = '/supplier'


export const PURCHASE_ORDER = '/purchase-order'
export const PURCHASE_INBOUND = '/purchase-inbound'
export const PURCHASE_RETURN = '/purchase-return'
export const PURCHASE_RETURN_OUTBOUND = '/purchase-return-outbound'
export const PAYMENT = '/payment'


export const SALES = '/sales'
export const SALES_OUTBOUND = '/sales-outbound'
export const SALES_INBOUND = '/sales-inbound'
export const SALES_RETURN = '/sales-return'
export const SALES_RETURN_INBOUND = '/sales-return-inbound'
export const COUNTER = '/counter'
