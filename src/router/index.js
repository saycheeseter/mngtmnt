import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/pages/Dashboard'
import Main from '@/pages/Main'
import Product from '@/pages/Product'
import Brand from '@/pages/Brand'
import Login from '@/pages/Login'

import { 
  PRODUCT,
  BRAND,
  BRANCH,
  CATEGORY,
  SUPPLIER,

  PURCHASE_ORDER,
  PURCHASE_INBOUND,
  PURCHASE_RETURN,
  PURCHASE_RETURN_OUTBOUND,
  PAYMENT,

  SALES,
  SALES_OUTBOUND,
  SALES_INBOUND,
  SALES_RETURN,
  SALES_RETURN_INBOUND,
  COUNTER
} from './routes.type'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: PRODUCT,
      name: 'product',
      component: Product
    },
    {
      path: BRAND,
      name: 'brand',
      component: Brand
    },
  ]
})
