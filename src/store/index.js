import Vue from 'vue'
import Vuex from 'vuex'
import posts from './modules/posts'
import links from './modules/links'
import login from './modules/login'
import tabs from './modules/tabs'
import {mutations , STORAGE_KEY } from './modules/tabs'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {
        posts,
        links,
        login,
        tabs
    },
    strict: debug,
})