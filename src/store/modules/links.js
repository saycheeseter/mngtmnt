import axios from 'axios';
import navLinks from './../../router/navLinks'
import { link } from 'fs';
import Router from './../../router/index'


const state = {
    all:[],
    tabs: JSON.parse(window.localStorage.getItem('tabs') || '[]'),
    customLinks:[],
    sideNavLinks:[],
}

const getters = {
    currentNavPresented({state}, link) {

    }
}

const actions = {
    getAllLinks({commit}) {
        navLinks.links (links => {
            commit('setNavLinks', links)
        })
        // navLinks.getLinks(links => {

        //     commit('setLinks', links)
        // })
    },
    // getAllCustomLinks({commit}) {
    //     navLinks.getCustomLinks(links => {
    //         commit('setCustomLinks', links)
    //     })
    // },
    addToTab({commit, state}, currentLink){
        console.log(currentLink)
    let currentTabArr = JSON.parse(window.localStorage.getItem('tabs'))
    let existing = false
        if("tabs" in localStorage){
            currentTabArr = JSON.parse(window.localStorage.getItem('tabs'))
            currentTabArr.forEach(el => {
                if(el.path === currentLink.path){
                    existing = true
                }
                
            })
            if(!existing){
                commit('setTab',currentLink)
            }
        }
        else {
            commit('setTab',currentLink)
        }
    }
}

const mutations = {
    setLinks (state, links) {
        state.all = links
        },
    setCustomLinks (state, customLinks) {
        state.customLinks = customLinks
        },
    setNavLinks (state, links) {
        state.sideNavLinks = links
        },
    setTab (state,tab) {
        state.tabs.push(tab)
        window.localStorage.setItem('tabs',JSON.stringify(state.tabs))
    },
    removeTab(state,tab){
        let len = state.tabs.length;
        // Router.push(`/asd`)
        for(let i=0; i<len; i++){
            if (state.tabs[i].path == tab.path) {
                
                console.log(Router.resolve(location).location.path)
                console.log(state.tabs.findIndex(x => x.page === state.tabs[i].page))
                console.log(len + " asd");
                
                console.log(state.tabs[i].path);
                
                // if(state.tabs[i-1].page)
                // Router.push(`${state.tabs[i-1].page}`)

                if(state.tabs.findIndex(x => x.path === state.tabs[i].path) == 0 && len != 0){
                    console.log(len)
                    if(len == 1){
                        console.log('none');
                        
                    }else if(len != 0 && state.tabs[1].path == Router.resolve(location).location.path ){
                        Router.push(`${state.tabs[1].path}`)
                    }
                    console.log('index 0');
                    
                }
                else if(Router.resolve(location).location.path == state.tabs[i].path){
                    console.log('same')
                    Router.push(`${state.tabs[i-1].path}`)
                }
                // else if(len == 0){
                //     Router.push(`${state.tabs[i-1].page}`)
                //     console.log('same')
                // }
                else{
                    console.log('not same');
                    
                    // Router.back()
                    // Router.push(`${state.tabs[i-1].page}`)
                }
                console.log('umaabot')
                state.tabs.splice(i,1)
                window.localStorage.setItem('tabs',JSON.stringify(state.tabs))
                break
            }

        }
        // $this.$router.push('/about')
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}