import { STORAGE_KEY } from './tabs'
import tabs from './tabs'

const localStoragePlugin = store => {
    store.subscribe((mutation, { tabs }) => {
        window.localStorage.setItem(STORAGE_KEY, JSON.stringify(tabs))
    })
}
export default process.env.NODE_ENV !== 'production'
    ? [localStoragePlugin]
    : [localStoragePlugin]