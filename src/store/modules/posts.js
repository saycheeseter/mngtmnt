import axios from 'axios';

const state = {
    all:[]
}

const getters = {}

const actions = {
    getAllPosts({commit}) {
        axios.get(`http://jsonplaceholder.typicode.com/posts`)
            .then(posts => {
                commit('setPosts', posts.data)
            })
            .catch(e => {
            // this.errors.push(e)
            })
    },
}

const mutations = {
    setPosts (state, posts) {
        state.all = posts
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}