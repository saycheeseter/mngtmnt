
export const STORAGE_KEY = 'tabs-current'
import plugins from './plugins'

const state = {
    tabs:JSON.parse(window.localStorage.getItem('tabs') || '[]')
}

const getters = {

}

const actions = {
    closeTab({commit,rootState}, tab) {
        // console.log(rootState.links.tabs)
        commit('links/removeTab', tab, { root: true })
    }
}

const mutations = {
    removeTab({commit,rootState},tab){
        rootState.links.tabs.splice(rootState.links.tabs.indexOf(tab), 1)
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations,
    plugins
}