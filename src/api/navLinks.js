import { 
    PRODUCT,
    BRAND,
    BRANCH,
    CATEGORY,
    SUPPLIER,

    PURCHASE_ORDER,
    PURCHASE_INBOUND,
    PURCHASE_RETURN,
    PURCHASE_RETURN_OUTBOUND,
    PAYMENT,

    SALES,
    SALES_OUTBOUND,
    SALES_INBOUND,
    SALES_RETURN,
    SALES_RETURN_INBOUND,
    COUNTER
} from './../router/routes.type'
const _navLinksAPI = [
    {"id": 1, "text": "Client", "page":"/client"},
    {"id": 2, "text": "Brand", "page":"/brand"},
    {"id": 3, "text": "Product","page":"/product"},
    {"id": 4, "text": "Dog", "page":"/dog"},
    {"id": 5, "text": "Cat", "page":"/cat"},
    {"id": 6, "text": "Mouse","page":"/mouse"},
    {"id": 7, "text": "Rabbit", "page":"/rabbit"},
    {"id": 8, "text": "Cow", "page":"/cow"},
    {"id": 9, "text": "Ant","page":"/ant"},

]
const _navLinksAPI2 = [
    {"id": 11, "text": "Clientasd", "page":"/clients"},
    {"id": 12, "text": "Brandasd", "page":"/brands"},
    {"id": 13, "text": "Productasd","page":"/products"},
    {"id": 14, "text": "Dogasd", "page":"/dogs"},
    {"id": 15, "text": "Catasd", "page":"/cats"},
    {"id": 16, "text": "Mouseasd","page":"/mouses"},
    {"id": 17, "text": "Rabbitasd", "page":"/rabbits"},
    {"id": 18, "text": "Cowasd", "page":"/cows"},
    {"id": 19, "text": "Antasd","page":"/ants"},
]

const _navLinks = [
    {
        label:'Data',
        icon:'',
        content:[
            {
                path:PRODUCT,
                label:'Product',
                icon:'',
                content:[]
            },
            {
                path:BRAND,
                label:'Brand',
                icon:'',
                content:[],
            },
            {
                path:BRANCH,
                label:'Branch',
                icon:'',
                content:[],
            },
            {
                path:CATEGORY,
                label:'Category',
                icon:'',
                content:[],
            },
            {
                path:SUPPLIER,
                label:'Supplier',
                icon:'',
                content:[],
            }
        ],
    },
    {
        label:'Purchase and Other Payment',
        icon:'',
        content:[
            {
                path:PURCHASE_ORDER,
                label:'Purchase Order',
                icon:'',
                content:[],
            },
            {
                path:PURCHASE_INBOUND,
                label:'Purchase Inbound',
                icon:'',
                content:[],
            },
            {
                path:PURCHASE_RETURN,
                label:'Purchase Return',
                icon:'',
                content:[],
            },
            {
                path:PURCHASE_RETURN_OUTBOUND,
                label:'Purchase Return Outbound',
                icon:'',
                content:[],
            },
            {
                path:PAYMENT,
                label:'Payment',
                icon:'',
                content:[],
            }
        ],
    },
    {
        label:'Sales and Other Income ',
        icon:'',
        content:[
            {
                path:SALES,
                label:'Sales',
                icon:'',
                content:[],
            },
            {
                path:SALES_OUTBOUND,
                label:'Sales Outbound',
                icon:'',
                content:[],
            },
            {
                path:SALES_INBOUND,
                label:'Purchase Inbound',
                icon:'',
                content:[],
            },
            {
                path:SALES_RETURN_INBOUND,
                label:'Sales Return Inbound',
                icon:'',
                content:[],
            },
            {
                path:COUNTER,
                label:'Counter',
                icon:'',
                content:[],
            }
        ],
    },

]

export default {
    getLinks (cb) {
        setTimeout(() => cb(_navLinksAPI), 100)
    },
    getCustomLinks (cb) {
        setTimeout(() => cb(_navLinksAPI2), 100)
    },
    links (cb) {
        setTimeout(() => cb(_navLinks), 100)
    },
}