// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import VueI18n from 'vue-i18n'
import localeLang from './lang/lang'

Vue.use(VueI18n)
Vue.config.productionTip = false

const i18n = new VueI18n({
    locale: window.localStorage.getItem('lang') || 'en',
    messages: localeLang, 
})

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    i18n,
    localeLang,
    components: { App },
    template: '<App/>'
})
